import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OauthPage } from './oauth';

@NgModule({
  declarations: [
    OauthPage,
  ],
  imports: [
    IonicPageModule.forChild(OauthPage),
  ],
})
export class OauthPageModule {}
