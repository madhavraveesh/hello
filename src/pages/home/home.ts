import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import firebase from 'firebase';
import { DashPage } from '../dash/dash';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
 userProfile: any = null;
  constructor(public navCtrl: NavController, public facebook: Facebook) {}
  login(): void {
    this.facebook.login(['email']).then( (response) => {
      const facebookCredential = firebase.auth.FacebookAuthProvider
        .credential(response.authResponse.accessToken);

      firebase.auth().signInWithCredential(facebookCredential)
        .then((success) => {
          console.log("Firebase success: " + JSON.stringify(success));
          this.navCtrl.push(DashPage);
          this.userProfile = success;
        })
        .catch((error) => {
          console.log("Firebase failure: " + JSON.stringify(error));
      });
      
    }).catch((error) => { console.log(error) });
  }
}

